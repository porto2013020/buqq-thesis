module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'plugin:vue/vue3-essential',
    'standard'
  ],
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: [
    'vue'
  ],
  rules: {
    'no-console': process.env.VUE_APP_NODE_ENV !== 'localhost' ? 'error' : 'warn',
    'no-debugger': 'error',
    'space-in-parens': ['off'],
    quotes: ['error', 'single'],
    'no-undef': ['off'],
    'computed-property-spacing': ['off'],
    'linebreak-style': ['off'],
    'brace-style': [2, 'stroustrup', { allowSingleLine: false }],
    'comma-dangle': ['error', 'never'],
    'no-param-reassign': ['error', { props: false }],
    semi: ['error', 'never'],
    camelcase: ['error', { properties: 'always' }],
    'function-paren-newline': ['off'],
    'implicit-arrow-linebreak': ['off'],
    'prefer-arrow-callback': ['off'],
    'arrow-body-style': ['off'],
    'no-return-assign': ['off'],
    'array-callback-return': ['off'],
    'prefer-destructuring': ['off'],
    'no-tabs': ['off'],
    'no-mixed-spaces-and-tabs': ['off'],
    'class-methods-use-this': ['off'],
    'no-useless-escape': ['off'],
    radix: ['error', 'as-needed'],
    'import/no-cycle': ['off']
  }
}
