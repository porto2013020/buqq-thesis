<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// guest can access
Route::get('/', function () {
  return Inertia::render('Welcome');
})->name('home');

Route::get('/home', function () {
  return Inertia::render('Welcome');
})->name('home');

Route::get('/marketplace', function () {
  return Inertia::render('Marketplace');
})->name('marketplace');

Route::get('/genre', function () {
  return Inertia::render('Genre');
})->name('genre');

Route::get('/genre/{genre}', function ($genre) {
  $genreId = request()->query('genreId');
  $genreTitle = request()->query('genreTitle');

  return Inertia::render('Book')
    ->with('genre', $genre)
    ->with('genreId', $genreId)
    ->with('genreTitle', $genreTitle);
})->name('genre.genre');

// must be authenticated to access
Route::get('/dashboard', function () {
  return Inertia::render('Authenticated/Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/publish', function () {
  return Inertia::render('Authenticated/Publish');
})->middleware(['auth', 'verified'])->name('publish');

// Route::get('/cart', function () {
//   return Inertia::render('Authenticated/Cart');
// })->middleware(['auth', 'verified'])->name('cart');

Route::get('/book-detail/{book}', function ($book) {
  $data = request()->query('data');
  $bookTitle = request()->query('bookTitle');

  return Inertia::render('Authenticated/BookDetail')
    ->with('book', $book)
    ->with('data', $data)
    ->with('bookTitle', $bookTitle);
})->middleware(['auth', 'verified'])->name('detail');

Route::get('/book-file/{file}', function ($file) {
  $data = request()->query('data');

  return Inertia::render('Authenticated/BookFile')
    ->with('file', $file)
    ->with('data', $data);
})->middleware(['auth', 'verified'])->name('file');

Route::middleware('auth')->group(function () {
  Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
  Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
  Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
