<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\TransactionController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  return $request->user();
});

Route::get('get-genre', [GenreController::class, 'getGenre']);
Route::get('get-book', [BookController::class, 'getBook']);
Route::get('get-collection', [TransactionController::class, 'getCollection']);

Route::post('publish-book', [BookController::class, 'publishBook'])->middleware(['auth']);
Route::post('purchase-book', [BookController::class, 'purchaseBook'])->middleware(['auth']);
Route::post('read-book', [BookController::class, 'readBook'])->middleware(['auth']);

require __DIR__ . '/auth.php';
